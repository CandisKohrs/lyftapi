## PHP wrapper around Lyft's APIs. ##

[Lyft APIs](https://developer.lyft.com/docs)

Get your Lyft *client id* and *client secret* [here](https://www.lyft.com/developers/manage).

Example usage,

```
$clientId = 'your client id';
$clientSecret = 'your client secret';

$lyftAPI = new LyftAPI($clientId, $clientSecret);
$qs = http_build_query([
  'lat' => 37.7739,
  'lng' => -122.4312,
  'ride_type' => 'lyft_line',
]);
$rideTypes = $lyftAPI->get('ridetypes', $qs);
```

Lyft is an online transportation company of United States and spreading all around the World with efficient growth in users. OnDemandly is a part this project in which it provides the promo codes guides( http://ondemandly.com/lyft-promo-code ) to the users of Lyft without any cost. Users can also get coupons for Lyft services on OnDemandly.